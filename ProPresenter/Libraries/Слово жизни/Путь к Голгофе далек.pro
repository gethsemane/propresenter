
(
����"19041""	117768465&
$34e5afe7-d8eb-4184-9819-071df925255a%Путь к Голгофе далек"�ȡ�*Ǣ��B
%  �?R&
$2ea8cdea-0ea4-4521-bae9-8a774f0acf3fb�
@
&
$4ba676e9-e6e1-423d-a05c-f785186f7425Group  �?  �?  �?&
$58eccb8d-f84a-4d8c-9153-f9db4dfb9322&
$ec2da5a3-98c5-46cb-accf-cc3a0bd18491&
$48b684e1-0ee1-4f46-8446-527f97cd23c3&
$bb3b4883-2579-4d7b-99fd-f13c5c9cc6ee&
$ba32af69-337f-4b0a-a665-2064a877daa0&
$73baa07c-626e-4d1e-9ee5-1a87cb8de042&
$aa83f58e-ea31-44e3-8772-d577d2df9c96&
$53c3d1ce-cb64-40e8-bc1b-91bea5038d31&
$5f36e592-2d0c-467d-88ed-f2d4be7c9074&
$40ac29cf-61f9-45bd-b992-ff712d3b07fc&
$37e984ba-a437-4e5d-a01b-3107d105563b&
$856b6726-46bc-4562-9cc3-17b7b1713955j�
&
$58eccb8d-f84a-4d8c-9153-f9db4dfb9322"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$4ffb14f6-ad5b-4eb9-bc26-4d8f9c1c25fd0H���
�
�
�
&
$cbf5e5a1-14c9-4c9e-92e7-840fe1e07c12
		��8��2@	r~v�Wӎ@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�o
)
Arial-BoldMT     @P@@JArialRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19728\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs130\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2}0B Hr  *%  �?2	      �@      �@:&
$538d0dc6-1ec6-4a0f-8eff-5a4a3307c30e�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$54082841-d8f9-4bca-b912-34830ee0711aAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$ec2da5a3-98c5-46cb-accf-cc3a0bd18491 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$5ee33836-9ced-49a4-b3fb-b9c2f8c184a40H���
�
�
�
&
$09298b38-1763-4cc3-8b72-2afb6ffc0b70
		��8��2@	r~v�Wӎ@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19728\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1055 ?\u1091 ?\u1090 ?\u1100 ? \u1082 ? \u1043 ?\u1086 ?\u1083 ?\u1075 ?\u1086 ?\u1092 ?\u1077 ? \u1076 ?\u1072 ?\u1083 ?\u1077 ?\u1082 ?, \u1082 ?\u1088 ?\u1086 ?\u1074 ?\u1100 ? \u1089 ?\u1090 ?\u1077 ?\u1082 ?\u1072 ?\u1083 ?\u1072 ? \u1080 ?\u1079 ? \u1088 ?\u1072 ?\u1085 ?,\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1057 ?\u1082 ?\u1086 ?\u1083 ?\u1100 ?\u1082 ?\u1086 ? \u1084 ?\u1091 ?\u1082 ? \u1080 ? \u1089 ?\u1090 ?\u1088 ?\u1072 ?\u1076 ?\u1072 ?\u1085 ?\u1080 ?\u1081 ? \u1074 ?\u1086 ? \u1074 ?\u1079 ?\u1086 ?\u1088 ?\u1077 ?!}0B Hr  *%  �?2	      �@      �@:&
$be2a69a7-cd06-4aa6-8600-b37f58406e07�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$1b51a7a4-ab1b-4005-b907-d28ec3343ceeAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$48b684e1-0ee1-4f46-8446-527f97cd23c3 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$3c3ea131-5062-4b86-ba41-a28b40e320bd0H���
�
�
�
&
$dbb73933-7583-474e-b2fc-22c459c0c8b1
		(���71Z@	v��s�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw16289\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1048 ?\u1080 ?\u1089 ?\u1091 ?\u1089 ? \u1076 ?\u1086 ?\u1088 ?\u1086 ?\u1075 ?\u1086 ?\u1081 ? \u1091 ?\u1084 ?\u1080 ?\u1088 ?\u1072 ?\u1083 ? \u1085 ?\u1072 ? \u1082 ?\u1088 ?\u1077 ?\u1089 ?\u1090 ?\u1077 ?.\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1055 ?\u1088 ?\u1080 ?\u1085 ?\u1103 ?\u1083 ? \u1074 ?\u1089 ?\u1077 ?: \u1080 ? \u1089 ?\u1090 ?\u1088 ?\u1072 ?\u1076 ?\u1072 ?\u1085 ?\u1100 ?\u1077 ?, \u1080 ? \u1075 ?\u1086 ?\u1088 ?\u1077 ?.}0B Hr  *%  �?2	      �@      �@:&
$dc5c1676-2c76-49e8-993d-876f5b6b3ce0�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$a6a669cf-dfcb-41fc-a956-df76ec1513ffAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$bb3b4883-2579-4d7b-99fd-f13c5c9cc6ee 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$7c0ba883-6f05-41dc-b044-959a5b133f260H���
�
�
�
&
$639a6941-dff3-469e-90d6-5b6a196ca21b
		J�E1�a@	�,]g:�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j 9      @b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw14865\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd10\expndtw50\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1053 ?\u1080 ?\u1082 ?\u1086 ?\u1075 ?\u1076 ?\u1072 ? \u1085 ?\u1077 ? \u1089 ?\u1084 ?\u1086 ?\u1075 ?\u1091 ?\expnd0\expndtw0  \expnd-8\expndtw-40\u1103 ? \u1079 ?\u1072 ?\u1073 ?\u1099 ?\u1090 ?\u1100 ? \u1101 ?\u1090 ?\u1086 ?\u1090 ? \u1082 ?\u1088 ?\u1077 ?\u1089 ?\u1090 ? -\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1054 ?\u1085 ? \u1089 ?\u1090 ?\u1086 ?\u1080 ?\u1090 ? \u1084 ?\u1077 ?\u1078 ?\u1076 ?\u1091 ? \u1085 ?\u1077 ?\u1073 ?\u1086 ?\u1084 ?, \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1105 ?\u1102 ?.}0B Hr  *%  �?2	      �@      �@:&
$8c25ed06-c531-47ad-8073-581338ef6860�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$d98f5e89-79fc-4b43-9a65-5a8e92727d81Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$ba32af69-337f-4b0a-a665-2064a877daa0 1 1 2"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$d31ffcbf-0625-4313-bfbd-3def04ee80920H���
�
�
�
&
$8eabb87b-792d-4c70-8dcd-80b03e5904a9
		��j2�0@	PI߼��@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19819\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1057 ?\u1074 ?\u1086 ?\u1077 ?\u1081 ? \u1089 ?\u1084 ?\u1077 ?\u1088 ?\u1090 ?\u1100 ?\u1102 ? \u1061 ?\u1088 ?\u1080 ?\u1089 ?\u1090 ?\u1086 ?\u1089 ? \u1087 ?\u1088 ?\u1080 ?\u1084 ?\u1080 ?\u1088 ?\u1080 ?\u1083 ? \u1085 ?\u1072 ?\u1089 ? \u1089 ? \u1058 ?\u1074 ?\u1086 ?\u1088 ?\u1094 ?\u1086 ?\u1084 ?,\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1048 ?\u1089 ?\u1082 ?\u1091 ?\u1087 ?\u1080 ?\u1083 ? \u1076 ?\u1088 ?\u1072 ?\u1075 ?\u1086 ?\u1094 ?\u1077 ?\u1085 ?\u1085 ?\u1086 ?\u1102 ? \u1082 ?\u1088 ?\u1086 ?\u1074 ?\u1100 ?\u1102 ?.}0B Hr  *%  �?2	      �@      �@:&
$edacdcd3-86b6-41df-a265-9c8d7ca0b3ab�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$d11d4249-3b06-4d35-9741-7c58b779ac6cAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$73baa07c-626e-4d1e-9ee5-1a87cb8de042 1 1 2 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$097b0423-16fe-4124-95e2-9faec8bb125b0H���
�
�
�
&
$4fa765e6-9d1d-4df4-8c40-0d981797f9da
		@���#�7@	�`W�ͅ�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�

0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j 9      �?b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19534\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd2\expndtw10\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1054 ?, \u1074 ?\u1085 ?\u1077 ?\u1084 ?\u1083 ?\u1080 ?, \u1095 ?\u1077 ?\u1083 ?\u1086 ?\u1074 ?\u1077 ?\u1082 ?,\expnd0\expndtw0  \u1075 ?\u1083 ?\u1072 ?\u1089 ?\u1091 ? \u1087 ?\u1077 ?\u1089 ?\u1085 ?\u1080 ? \u1084 ?\u1086 ?\u1077 ?\u1081 ?.\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd-6\expndtw-30\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1063 ?\u1090 ?\u1086 ? \u1090 ?\u1099 ? \u1084 ?\u1077 ?\u1076 ?\u1083 ?\u1080 ?\u1096 ?\u1100 ?, \u1084 ?\u1086 ?\u1081 ? \u1076 ?\u1088 ?\u1091 ?\u1075 ?,\expnd0\expndtw0  \u1095 ?\u1090 ?\u1086 ? \u1090 ?\u1099 ? \u1078 ?\u1076 ?\u1077 ?\u1096 ?\u1100 ?!}0B Hr  *%  �?2	      �@      �@:&
$811b457b-dc6d-404d-9500-ae0bf811b897�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$758f2196-7611-4a4c-9247-82a210d82008Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$aa83f58e-ea31-44e3-8772-d577d2df9c96
 1 1 2 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$b3b016df-c73b-43a9-b0c1-c85c1fe7dcf20H���
�
�
�
&
$8825e96a-3639-424f-87d0-e89a71de2887
		D�4 \S@	/����(�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw17382\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1063 ?\u1077 ?\u1084 ? \u1090 ?\u1099 ? \u1087 ?\u1083 ?\u1072 ?\u1090 ?\u1080 ?\u1096 ?\u1100 ? \u1045 ?\u1084 ?\u1091 ? \u1079 ?\u1072 ? \u1041 ?\u1086 ?\u1078 ?\u1077 ?\u1089 ?\u1090 ?\u1074 ?\u1077 ?\u1085 ?\u1085 ?\u1099 ?\u1081 ? \u1076 ?\u1072 ?\u1088 ?,\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1047 ?\u1072 ? \u1087 ?\u1088 ?\u1086 ?\u1083 ?\u1080 ?\u1090 ?\u1091 ?\u1102 ? \u1082 ?\u1088 ?\u1086 ?\u1074 ?\u1100 ? \u1095 ?\u1090 ?\u1086 ? \u1076 ?\u1072 ?\u1077 ?\u1096 ?\u1100 ??}0B Hr  *%  �?2	      �@      �@:&
$d47d27a2-f1f9-4232-b134-1561724ebd1b�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$90288cd3-fd12-40e1-a1af-36bb8fbf9ca8Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$53c3d1ce-cb64-40e8-bc1b-91bea5038d31 1 1 2 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$db155b5b-284a-489f-becb-51801a8e86830H���
�
�
�
&
$a1ad8615-3820-4c77-bebd-303d53f60081
		�n�5�-@	ȉ�S��@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�	{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19886\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1044 ?\u1072 ?\u1083 ?\u1100 ? \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1080 ?, \u1089 ?\u1074 ?\u1077 ?\u1090 ? \u1079 ?\u1072 ?\u1088 ?\u1080 ?, \expnd-14\expndtw-70\u1088 ?\u1072 ?\u1076 ?\u1086 ?\u1089 ?\u1090 ?\u1100 ? \u1076 ?\u1085 ?\u1077 ?\u1081 ?, \u1075 ?\u1088 ?\u1091 ?\u1089 ?\u1090 ?\u1100 ? \u1085 ?\u1086 ?\u1095 ?\u1077 ?\u1081 ? -\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1044 ?\u1083 ?\u1103 ? \u1090 ?\u1077 ?\u1073 ?\u1103 ? \u1101 ?\u1090 ?\u1086 ? \u1089 ?\u1086 ?\u1079 ?\u1076 ?\u1072 ?\u1083 ? \u1043 ?\u1086 ?\u1089 ?\u1087 ?\u1086 ?\u1076 ?\u1100 ?.}0B Hr  *%  �?2	      �@      �@:&
$3c784204-e71c-47a3-bf97-fed51cc7b886�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$e53ce241-2d7d-4643-881c-9047922007a1Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$5f36e592-2d0c-467d-88ed-f2d4be7c9074 1 1 2 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$897ced34-ec7e-4eb0-9575-5fb5b010f0c80H���
�
�
�
&
$72d8babc-599c-4220-936b-e3beae0cdda9
		D�4 \S@	/����(�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw17382\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1054 ?\u1085 ? \u1079 ?\u1086 ?\u1074 ?\u1077 ?\u1090 ?: "\u1054 ?, \u1087 ?\u1088 ?\u1080 ?\u1076 ?\u1080 ?!" - \u1084 ?\u1080 ?\u1084 ?\u1086 ?, \u1076 ?\u1088 ?\u1091 ?\u1075 ?, \u1085 ?\u1077 ? \u1087 ?\u1088 ?\u1086 ?\u1081 ?\u1076 ?\u1080 ?:\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1042 ? \u1053 ?\u1077 ?\u1084 ? \u1090 ?\u1099 ? \u1089 ?\u1095 ?\u1072 ?\u1089 ?\u1090 ?\u1100 ?\u1077 ? \u1080 ? \u1084 ?\u1080 ?\u1088 ? \u1086 ?\u1073 ?\u1088 ?\u1077 ?\u1090 ?\u1077 ?\u1096 ?\u1100 ?.}0B Hr  *%  �?2	      �@      �@:&
$00b3c27c-5f0a-4074-b446-2fe3192ea6ac�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$67a63f0c-1e61-4ea8-b6b0-ab7b38ea8035Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$40ac29cf-61f9-45bd-b992-ff712d3b07fc 1 1 2 1 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$77f56ac1-25cd-4851-810f-6854830270b50H���
�
�
�
&
$0d84e8b7-d1a9-4245-a224-c1d4cc3427e9
		��
��>@	�S��@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19249\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs148\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1044 ?\u1072 ?\u1081 ?, \u1043 ?\u1086 ?\u1089 ?\u1087 ?\u1086 ?\u1076 ?\u1100 ?, \u1084 ?\u1085 ?\u1077 ? \u1087 ?\u1086 ?\u1085 ?\u1103 ?\u1090 ?\u1100 ?\fs150  \u1090 ?\u1072 ?\u1081 ?\u1085 ?\u1091 ? \u1074 ?\u1077 ?\u1095 ?\u1085 ?\u1086 ?\u1081 ? \u1083 ?\u1102 ?\u1073 ?\u1074 ?\u1080 ?.\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1058 ?\u1086 ?\u1081 ?, \u1095 ?\u1090 ?\u1086 ? \u1058 ?\u1099 ? \u1085 ?\u1072 ?\u1084 ? \u1085 ?\u1072 ? \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1102 ? \u1087 ?\u1088 ?\u1080 ?\u1085 ?\u1077 ?\u1089 ?,}0B Hr  *%  �?2	      �@      �@:&
$67585daf-9093-4c86-a89d-79ab603ffc12�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$75813e0a-7f11-44f4-861a-ff98ab8c5ecbAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$37e984ba-a437-4e5d-a01b-3107d105563b 1 1 2 1 1 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$87a9ec21-b89f-4f82-ae19-81118109bba60H���
�
�
�
&
$a3288499-2a31-4314-87b9-84ed20f8556d
		�~\3��\@	S�(���@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j 9      �?b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw15840\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd4\expndtw20\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1048 ? \u1090 ?\u1086 ?\u1075 ?\u1076 ?\u1072 ? \u1103 ? \u1089 ?\u1087 ?\u1086 ?\u1082 ?\u1086 ?\u1081 ?\u1085 ?\u1086 ?\expnd0\expndtw0  \u1080 ? \u1090 ?\u1074 ?\u1077 ?\u1088 ?\u1076 ?\u1086 ? \u1087 ?\u1086 ?\u1081 ?\u1076 ?\u1091 ?\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd-4\expndtw-20\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1055 ?\u1086 ? \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1077 ? \u1089 ?\u1088 ?\u1077 ?\u1076 ?\u1080 ? \u1073 ?\u1091 ?\u1088 ?\u1100 ?,\expnd0\expndtw0  \u1089 ?\u1088 ?\u1077 ?\u1076 ?\u1080 ? \u1075 ?\u1088 ?\u1086 ?\u1079 ?!}0B Hr  *%  �?2	      �@      �@:&
$14e7eac9-e80f-43c6-8f8c-e001838f7dfa�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$212a7cfe-2432-48a5-8322-e50dd6dd9167Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$856b6726-46bc-4562-9cc3-17b7b1713955 1 1 2 1 1 1 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$def41d67-6953-4227-8092-8caac7ac30130H���
C*%  �?2	      �@      �@:&
$f4591469-0d59-4076-bc86-281332a3dbc5�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$e5f222ee-9ca1-4b45-bba6-89c810af649cAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`r � 