
(
����"19041""	117768465&
$278f72be-f9be-4326-a883-f93ab9eee167%Путь к Голгофе далек"����*Ƣ��B
%  �?R&
$bc50f617-fabf-4445-bb0a-3223caf680ebb�
@
&
$1c7af7b4-258c-4060-9416-4a9d0ec7ea1aGroup  �?  �?  �?&
$c911d2ef-3f51-49dd-9b1f-9fb918b3c0e1&
$dd210e6a-5543-42f2-8cce-7d898acf105b&
$6c2c8aff-1538-4aae-82db-e7b0fb52055b&
$d7e7c37a-d5ad-4c39-931e-e30282ec0298&
$aa5f0815-ac9b-4e3c-a755-f8b185f0306a&
$60cb8298-66ba-4c8d-8c55-f235531c4c92&
$29ffd646-ef66-48d1-8cb3-a1e6206600a4&
$6622130e-1588-4a62-b21b-52407f4f24f3&
$3f3dce2b-ca06-4cfd-98bb-b8b06bb0f9c5&
$f11b9b34-7cea-4e1c-a6d8-f44ac91708d6&
$3c6bdbaf-d196-4300-a57c-f08b51245cbd&
$8f7bdbfb-c744-4136-bb72-fb771df7408e&
$c98ca202-12e5-4c66-af74-be0769d85856&
$d66f1f05-420d-4e46-a58e-cf7eef8a5333&
$5cac3644-9e0e-4ccd-99dd-830ae10518fd&
$accc9f2f-c3d1-4c75-97bd-68ca632069d8j�
&
$c911d2ef-3f51-49dd-9b1f-9fb918b3c0e1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$9054b825-5172-4a4c-8d78-22f5007a59f30H���
�
�
�
&
$9e5ac9ed-4fa3-41dc-9a0d-d1e423a01df3
		��8��2@	r~v�Wӎ@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�o
)
Arial-BoldMT     @P@@JArialRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19728\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs130\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2}0B Hr  *%  �?2	      �@      �@:&
$8e0dac98-586f-40fe-abac-b1782bd733cd�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$98a72a3a-0a62-4cc8-995a-8af48321da22Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$dd210e6a-5543-42f2-8cce-7d898acf105b 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$99734495-a599-4a68-98ba-b78d679529ab0H���
�
�
�
&
$86a453cb-28d7-4879-a6f0-a1c953d1e861
		��8��2@	r~v�Wӎ@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19728\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1055 ?\u1091 ?\u1090 ?\u1100 ? \u1082 ? \u1043 ?\u1086 ?\u1083 ?\u1075 ?\u1086 ?\u1092 ?\u1077 ? \u1076 ?\u1072 ?\u1083 ?\u1077 ?\u1082 ?, \u1082 ?\u1088 ?\u1086 ?\u1074 ?\u1100 ? \u1089 ?\u1090 ?\u1077 ?\u1082 ?\u1072 ?\u1083 ?\u1072 ? \u1080 ?\u1079 ? \u1088 ?\u1072 ?\u1085 ?,\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1057 ?\u1082 ?\u1086 ?\u1083 ?\u1100 ?\u1082 ?\u1086 ? \u1084 ?\u1091 ?\u1082 ? \u1080 ? \u1089 ?\u1090 ?\u1088 ?\u1072 ?\u1076 ?\u1072 ?\u1085 ?\u1080 ?\u1081 ? \u1074 ?\u1086 ? \u1074 ?\u1079 ?\u1086 ?\u1088 ?\u1077 ?!}0B Hr  *%  �?2	      �@      �@:&
$7543360a-8c82-460e-9c19-58efe398980b�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$7c86de5a-3cf2-4781-8074-6ee08375d8c9Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$6c2c8aff-1538-4aae-82db-e7b0fb52055b 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$6ff41745-9836-4a1d-9951-ac9bdddd26550H���
�
�
�
&
$370b511e-8c05-4e6f-a776-860602645ed1
		(���71Z@	v��s�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw16289\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1048 ?\u1080 ?\u1089 ?\u1091 ?\u1089 ? \u1076 ?\u1086 ?\u1088 ?\u1086 ?\u1075 ?\u1086 ?\u1081 ? \u1091 ?\u1084 ?\u1080 ?\u1088 ?\u1072 ?\u1083 ? \u1085 ?\u1072 ? \u1082 ?\u1088 ?\u1077 ?\u1089 ?\u1090 ?\u1077 ?.\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1055 ?\u1088 ?\u1080 ?\u1085 ?\u1103 ?\u1083 ? \u1090 ?\u1103 ?\u1078 ?\u1082 ?\u1080 ?\u1077 ? \u1084 ?\u1091 ?\u1082 ?\u1080 ? \u1080 ? \u1075 ?\u1086 ?\u1088 ?\u1077 ?.}0B Hr  *%  �?2	      �@      �@:&
$2ad5c04b-5aca-40a1-bc16-21ce26ed67f2�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$768800e8-6271-4ad8-a755-49fc58c3452cAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$d7e7c37a-d5ad-4c39-931e-e30282ec0298 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$374194cf-b7ef-406a-9f76-662cb424dd9f0H���
�
�
�
&
$fb0f95c7-19ca-4be4-96f3-81e404dcca3e
		J�E1�a@	�,]g:�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j 9      @b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw14865\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd10\expndtw50\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1053 ?\u1080 ?\u1082 ?\u1086 ?\u1075 ?\u1076 ?\u1072 ? \u1085 ?\u1077 ? \u1089 ?\u1084 ?\u1086 ?\u1075 ?\u1091 ?\expnd0\expndtw0  \expnd-8\expndtw-40\u1103 ? \u1079 ?\u1072 ?\u1073 ?\u1099 ?\u1090 ?\u1100 ? \u1101 ?\u1090 ?\u1086 ?\u1090 ? \u1082 ?\u1088 ?\u1077 ?\u1089 ?\u1090 ? -\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1054 ?\u1085 ? \u1089 ?\u1090 ?\u1086 ?\u1080 ?\u1090 ? \u1084 ?\u1077 ?\u1078 ?\u1076 ?\u1091 ? \u1085 ?\u1077 ?\u1073 ?\u1086 ?\u1084 ?, \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1105 ?\u1102 ?.}0B Hr  *%  �?2	      �@      �@:&
$2a446a9e-4bd7-4145-9ce6-43a1d22b33b6�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$c5cb2a2d-794d-4785-b7d6-0309cb8f3cbfAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$aa5f0815-ac9b-4e3c-a755-f8b185f0306a 1 1 2"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$6185b037-af80-49ea-a58b-e87b5e3abbb40H���
�
�
�
&
$4ca819bb-a07a-42ce-b60c-13903f39f62b
		��j2�0@	PI߼��@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19819\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1057 ?\u1074 ?\u1086 ?\u1077 ?\u1081 ? \u1089 ?\u1084 ?\u1077 ?\u1088 ?\u1090 ?\u1100 ?\u1102 ? \u1061 ?\u1088 ?\u1080 ?\u1089 ?\u1090 ?\u1086 ?\u1089 ? \u1087 ?\u1088 ?\u1080 ?\u1084 ?\u1080 ?\u1088 ?\u1080 ?\u1083 ? \u1085 ?\u1072 ?\u1089 ? \u1089 ? \u1058 ?\u1074 ?\u1086 ?\u1088 ?\u1094 ?\u1086 ?\u1084 ?,\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1048 ?\u1089 ?\u1082 ?\u1091 ?\u1087 ?\u1080 ?\u1083 ? \u1076 ?\u1088 ?\u1072 ?\u1075 ?\u1086 ?\u1094 ?\u1077 ?\u1085 ?\u1085 ?\u1086 ?\u1102 ? \u1082 ?\u1088 ?\u1086 ?\u1074 ?\u1100 ?\u1102 ?.}0B Hr  *%  �?2	      �@      �@:&
$cde33cfb-19ec-43ce-bd73-fe337ba4d10b�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$a50f3e94-e904-4d4d-a8a4-e628c116d056Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$60cb8298-66ba-4c8d-8c55-f235531c4c92 1 1 2 1 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$a53d4034-99a0-4fde-a69b-959f14b1e6720H���
�
�
�
&
$e72980bc-2f5a-4780-8ef2-62c6acc83420
		��Κj[1@	�SVI�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19785\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1044 ?\u1072 ?\u1081 ?, \u1043 ?\u1086 ?\u1089 ?\u1087 ?\u1086 ?\u1076 ?\u1100 ?, \u1084 ?\u1085 ?\u1077 ? \u1087 ?\u1086 ?\u1085 ?\u1103 ?\u1090 ?\u1100 ? \u1090 ?\u1072 ?\u1081 ?\u1085 ?\u1091 ? \u1074 ?\u1077 ?\u1095 ?\u1085 ?\u1086 ?\u1081 ? \u1083 ?\u1102 ?\u1073 ?\u1074 ?\u1080 ?.\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1058 ?\u1086 ?\u1081 ?, \u1095 ?\u1090 ?\u1086 ? \u1058 ?\u1099 ? \u1085 ?\u1072 ?\u1084 ? \u1085 ?\u1072 ? \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1102 ? \u1087 ?\u1088 ?\u1080 ?\u1085 ?\u1077 ?\u1089 ?,}0B Hr  *%  �?2	      �@      �@:&
$3d0399f8-b026-49cd-bb66-aceaa4b28869�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$b17e81bc-14d9-43b7-bbbf-e78ac6f27164Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$29ffd646-ef66-48d1-8cb3-a1e6206600a4 1 1 2 1 1 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$e0669a79-213b-4d4a-8486-24430e9772940H���
�
�
�
&
$72057e4e-a0fa-4f0f-bd58-f7c0d28bc787
		�~\3��\@	S�(���@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j 9      �?b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw15840\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd4\expndtw20\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1048 ? \u1090 ?\u1086 ?\u1075 ?\u1076 ?\u1072 ? \u1103 ? \u1089 ?\u1087 ?\u1086 ?\u1082 ?\u1086 ?\u1081 ?\u1085 ?\u1086 ?\expnd0\expndtw0  \u1080 ? \u1090 ?\u1074 ?\u1077 ?\u1088 ?\u1076 ?\u1086 ? \u1087 ?\u1086 ?\u1081 ?\u1076 ?\u1091 ?\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd-4\expndtw-20\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1055 ?\u1086 ? \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1077 ? \u1089 ?\u1088 ?\u1077 ?\u1076 ?\u1080 ? \u1073 ?\u1091 ?\u1088 ?\u1100 ?,\expnd0\expndtw0  \u1089 ?\u1088 ?\u1077 ?\u1076 ?\u1080 ? \u1075 ?\u1088 ?\u1086 ?\u1079 ?!}0B Hr  *%  �?2	      �@      �@:&
$eda1eecc-5937-41d7-bfa7-6bad2b915833�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$b677f2d5-baaf-4c77-b143-48027571dd85Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$6622130e-1588-4a62-b21b-52407f4f24f3 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$2c887309-91b3-49b8-a85f-2a30a11744a90H���
�
�
�
&
$84381bc6-b2e6-4d27-bfcf-966e16d98f14
		J�E1�a@	�,]g:�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j 9      @b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw14865\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd10\expndtw50\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1053 ?\u1080 ?\u1082 ?\u1086 ?\u1075 ?\u1076 ?\u1072 ? \u1085 ?\u1077 ? \u1089 ?\u1084 ?\u1086 ?\u1075 ?\u1091 ?\expnd0\expndtw0  \expnd-8\expndtw-40\u1103 ? \u1079 ?\u1072 ?\u1073 ?\u1099 ?\u1090 ?\u1100 ? \u1101 ?\u1090 ?\u1086 ?\u1090 ? \u1082 ?\u1088 ?\u1077 ?\u1089 ?\u1090 ? -\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1054 ?\u1085 ? \u1089 ?\u1090 ?\u1086 ?\u1080 ?\u1090 ? \u1084 ?\u1077 ?\u1078 ?\u1076 ?\u1091 ? \u1085 ?\u1077 ?\u1073 ?\u1086 ?\u1084 ?, \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1105 ?\u1102 ?.}0B Hr  *%  �?2	      �@      �@:&
$1d37bfff-46a8-4f78-b44f-e7d99dc4e650�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$7bf91daf-ab6f-40e8-b2f2-e060668f8d67Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$3f3dce2b-ca06-4cfd-98bb-b8b06bb0f9c5 1 1 2 2"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$f06ff974-1fb0-477d-b611-ffb8cb0e6d770H���
�
�
�
&
$f318756f-e9f3-4f7e-a20b-72537b80e08f
		��j2�0@	PI߼��@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19819\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1057 ?\u1074 ?\u1086 ?\u1077 ?\u1081 ? \u1089 ?\u1084 ?\u1077 ?\u1088 ?\u1090 ?\u1100 ?\u1102 ? \u1061 ?\u1088 ?\u1080 ?\u1089 ?\u1090 ?\u1086 ?\u1089 ? \u1087 ?\u1088 ?\u1080 ?\u1084 ?\u1080 ?\u1088 ?\u1080 ?\u1083 ? \u1085 ?\u1072 ?\u1089 ? \u1089 ? \u1058 ?\u1074 ?\u1086 ?\u1088 ?\u1094 ?\u1086 ?\u1084 ?,\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1048 ?\u1089 ?\u1082 ?\u1091 ?\u1087 ?\u1080 ?\u1083 ? \u1076 ?\u1088 ?\u1072 ?\u1075 ?\u1086 ?\u1094 ?\u1077 ?\u1085 ?\u1085 ?\u1086 ?\u1102 ? \u1082 ?\u1088 ?\u1086 ?\u1074 ?\u1100 ?\u1102 ?.}0B Hr  *%  �?2	      �@      �@:&
$7bae22d1-b409-46c1-9389-246a7b0ba551�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$53841d07-3083-4417-be09-766a06df4042Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$f11b9b34-7cea-4e1c-a6d8-f44ac91708d6 1 1 2 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$0ad3ab5e-4a61-495a-b2f5-789ba421d16a0H���
�
�
�
&
$b58c2ebf-8520-40eb-b726-25103335de27
		@���#�7@	�`W�ͅ�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�

0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j 9      �?b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19534\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd2\expndtw10\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1054 ?, \u1074 ?\u1085 ?\u1077 ?\u1084 ?\u1083 ?\u1080 ?, \u1095 ?\u1077 ?\u1083 ?\u1086 ?\u1074 ?\u1077 ?\u1082 ?,\expnd0\expndtw0  \u1075 ?\u1083 ?\u1072 ?\u1089 ?\u1091 ? \u1087 ?\u1077 ?\u1089 ?\u1085 ?\u1080 ? \u1084 ?\u1086 ?\u1077 ?\u1081 ?.\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd-6\expndtw-30\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1063 ?\u1090 ?\u1086 ? \u1090 ?\u1099 ? \u1084 ?\u1077 ?\u1076 ?\u1083 ?\u1080 ?\u1096 ?\u1100 ?, \u1084 ?\u1086 ?\u1081 ? \u1076 ?\u1088 ?\u1091 ?\u1075 ?,\expnd0\expndtw0  \u1095 ?\u1090 ?\u1086 ? \u1090 ?\u1099 ? \u1078 ?\u1076 ?\u1077 ?\u1096 ?\u1100 ?!}0B Hr  *%  �?2	      �@      �@:&
$87572527-76ad-499f-8189-f960eb5da440�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$5b4f23d2-fe7e-438c-a21e-a8ca7e60dda9Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$3c6bdbaf-d196-4300-a57c-f08b51245cbd
 1 1 2 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$106c86ab-3d22-4226-9dcd-605cd41093b90H���
�
�
�
&
$bb384ec8-3f49-428c-9681-cde79584ff60
		D�4 \S@	/����(�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw17382\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1063 ?\u1077 ?\u1084 ? \u1090 ?\u1099 ? \u1087 ?\u1083 ?\u1072 ?\u1090 ?\u1080 ?\u1096 ?\u1100 ? \u1045 ?\u1084 ?\u1091 ? \u1079 ?\u1072 ? \u1041 ?\u1086 ?\u1078 ?\u1077 ?\u1089 ?\u1090 ?\u1074 ?\u1077 ?\u1085 ?\u1085 ?\u1099 ?\u1081 ? \u1076 ?\u1072 ?\u1088 ?,\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1047 ?\u1072 ? \u1087 ?\u1088 ?\u1086 ?\u1083 ?\u1080 ?\u1090 ?\u1091 ?\u1102 ? \u1082 ?\u1088 ?\u1086 ?\u1074 ?\u1100 ? \u1095 ?\u1090 ?\u1086 ? \u1076 ?\u1072 ?\u1077 ?\u1096 ?\u1100 ??}0B Hr  *%  �?2	      �@      �@:&
$147b68fa-b96b-493e-9ab1-009eed38bdc5�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$02537b9e-88ee-4016-8475-acc869c38641Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$8f7bdbfb-c744-4136-bb72-fb771df7408e 1 1 2 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$de39cfb8-6ea5-4e20-abcf-a35573a8e1be0H���
�
�
�
&
$2f23e3b6-83ed-4478-b7ce-dcdbd781f8a9
		D�4 \S@	/����(�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw17382\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1044 ?\u1072 ?\u1083 ?\u1100 ? \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1080 ?, \u1089 ?\u1074 ?\u1077 ?\u1090 ? \u1079 ?\u1072 ?\u1088 ?\u1080 ?, \u1073 ?\u1083 ?\u1072 ?\u1075 ?\u1086 ? \u1076 ?\u1085 ?\u1077 ?\u1081 ? \u1074 ?\u1087 ?\u1077 ?\u1088 ?\u1077 ?\u1076 ?\u1080 ? -\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1044 ?\u1083 ?\u1103 ? \u1090 ?\u1077 ?\u1073 ?\u1103 ? \u1101 ?\u1090 ?\u1086 ? \u1089 ?\u1086 ?\u1079 ?\u1076 ?\u1072 ?\u1083 ? \u1043 ?\u1086 ?\u1089 ?\u1087 ?\u1086 ?\u1076 ?\u1100 ?.}0B Hr  *%  �?2	      �@      �@:&
$784d95e3-6360-4349-bb0f-d2990f87b6c2�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$c1be4f2e-7092-4b0a-b9d7-8c132fcf6555Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$c98ca202-12e5-4c66-af74-be0769d85856 1 1 2 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$3b9e7dfc-1ff7-4575-a1fb-d306e0f2ed0c0H���
�
�
�
&
$bf2c3d73-bc98-45cc-901a-209b779349a8
		D�4 \S@	/����(�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw17382\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1054 ?\u1085 ? \u1079 ?\u1086 ?\u1074 ?\u1077 ?\u1090 ?: "\u1054 ?, \u1087 ?\u1088 ?\u1080 ?\u1076 ?\u1080 ?!" - \u1084 ?\u1080 ?\u1084 ?\u1086 ?, \u1076 ?\u1088 ?\u1091 ?\u1075 ?, \u1085 ?\u1077 ? \u1087 ?\u1088 ?\u1086 ?\u1081 ?\u1076 ?\u1080 ?:\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1042 ? \u1053 ?\u1077 ?\u1084 ? \u1090 ?\u1099 ? \u1089 ?\u1095 ?\u1072 ?\u1089 ?\u1090 ?\u1100 ?\u1077 ? \u1080 ? \u1084 ?\u1080 ?\u1088 ? \u1086 ?\u1073 ?\u1088 ?\u1077 ?\u1090 ?\u1077 ?\u1096 ?\u1100 ?.}0B Hr  *%  �?2	      �@      �@:&
$5f732edf-7b06-42a5-9752-075bafe2e8e5�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$db2e2ae2-2ba4-41d0-9504-1a3f83bc3836Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$d66f1f05-420d-4e46-a58e-cf7eef8a5333 1 1 1 2"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$859e511c-c582-4c18-ad84-0e777edaf1fe0H���
�
�
�
&
$27fd8930-fcf0-470f-9f4e-0894298140a0
		J�E1�a@	�,]g:�@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�	
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j 9      @b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw14865\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd10\expndtw50\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1053 ?\u1080 ?\u1082 ?\u1086 ?\u1075 ?\u1076 ?\u1072 ? \u1085 ?\u1077 ? \u1089 ?\u1084 ?\u1086 ?\u1075 ?\u1091 ?\expnd0\expndtw0  \expnd-8\expndtw-40\u1103 ? \u1079 ?\u1072 ?\u1073 ?\u1099 ?\u1090 ?\u1100 ? \u1101 ?\u1090 ?\u1086 ?\u1090 ? \u1082 ?\u1088 ?\u1077 ?\u1089 ?\u1090 ? -\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1054 ?\u1085 ? \u1089 ?\u1090 ?\u1086 ?\u1080 ?\u1090 ? \u1084 ?\u1077 ?\u1078 ?\u1076 ?\u1091 ? \u1085 ?\u1077 ?\u1073 ?\u1086 ?\u1084 ?, \u1079 ?\u1077 ?\u1084 ?\u1083 ?\u1105 ?\u1102 ?.}0B Hr  *%  �?2	      �@      �@:&
$11a0b69b-ded3-409a-9479-ee24701921bf�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$92be398c-a33e-4ccb-afe0-b4fd6bf8c8edAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$5cac3644-9e0e-4ccd-99dd-830ae10518fd 1 1 2 3"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$994efe9e-7082-4b64-ab2d-cf73cf2361c00H���
�
�
�
&
$8a126164-98a1-4e65-976f-1c60b9666038
		��j2�0@	PI߼��@      �@)      �?B�
   !
		      �?		      �?		      �?<
	      �?      �?	      �?      �?	      �?      �?!
	      �?	      �?	      �?J
���=��?  �?%  �?R      @  �?  �?  �?%  �?Z+     �s@      @!      @*%  �?1      �?b	�������?j�
v
0
Arial-BoldMT     �R@@JArial-BoldMTRRegular  �?  �?  �?%  �?2)      �?A      $@j b  �?  �?  �?%  �?"+     �s@      @!      @*%  �?1      �?*�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil Arial-BoldMT;}{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw19819\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1057 ?\u1074 ?\u1086 ?\u1077 ?\u1081 ? \u1089 ?\u1084 ?\u1077 ?\u1088 ?\u1090 ?\u1100 ?\u1102 ? \u1061 ?\u1088 ?\u1080 ?\u1089 ?\u1090 ?\u1086 ?\u1089 ? \u1087 ?\u1088 ?\u1080 ?\u1084 ?\u1080 ?\u1088 ?\u1080 ?\u1083 ? \u1085 ?\u1072 ?\u1089 ? \u1089 ? \u1058 ?\u1074 ?\u1086 ?\u1088 ?\u1094 ?\u1086 ?\u1084 ?,\par\pard\li0\fi0\ri0\qc\sb0\sa0\sl240\slmult1\slleading200\f0\b\i0\ul0\strike0\fs150\expnd0\expndtw0\cf1\strokewidth0\strokec1\nosupersub\ulc0\highlight2\cb2\u1048 ?\u1089 ?\u1082 ?\u1091 ?\u1087 ?\u1080 ?\u1083 ? \u1076 ?\u1088 ?\u1072 ?\u1075 ?\u1086 ?\u1094 ?\u1077 ?\u1085 ?\u1085 ?\u1086 ?\u1102 ? \u1082 ?\u1088 ?\u1086 ?\u1074 ?\u1100 ?\u1102 ?.}0B Hr  *%  �?2	      �@      �@:&
$3e8838cf-97fa-48c2-9c8b-b2cc6ed53a7e�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$6c365f86-4f3a-484a-a88f-8eab8c497418Audience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`j�
&
$accc9f2f-c3d1-4c75-97bd-68ca632069d8 1 1 2 1 1 1 1 1 1 1"&
$00000000-0000-0000-0000-000000000000(2&
$00000000-0000-0000-0000-000000000000: R�
&
$d557f2c5-5d62-41ca-8c87-7a937a89b78e0H���
C*%  �?2	      �@      �@:&
$9ee3661d-17ca-4921-af6d-b9fbcb1dee2b�
�{\rtf0\ansi\ansicpg1252{\fonttbl\f0\fnil ArialMT;}{\colortbl;\red0\green0\blue0;\red255\green255\blue255;\red255\green255\blue255;}{\*\expandedcolortbl;\csgenericrgb\c0\c0\c0\c100000;\csgenericrgb\c100000\c100000\c100000\c100000;\csgenericrgb\c100000\c100000\c100000\c0;}{\*\listtable}{\*\listoverridetable}\uc1\paperw12240\margl0\margr0\margt0\margb0\pard\li0\fi0\ri0\ql\sb0\sa0\sl240\slmult1\slleading0\f0\b0\i0\ul0\strike0\fs100\expnd0\expndtw0\cf1\strokewidth0\strokec2\nosupersub\ulc0\highlight3\cb3}" R�
&
$c657479a-12cd-4452-b0f2-8d1d3107542bAudience Look0H�E
C
&
$1ad75b26-98d0-4b12-8ad6-9fe9c2f1e1f4подстрочник 50`r � 